package com.avifit.validator;

import com.avifit.dto.MemberDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class MemberValidatorTest {

    private static Validator validator;

    @BeforeAll
    public static void setup() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    @DisplayName("Should not violate any constraint when member is valid")
    void ifMemberIsValid_thenNoConstraintViolated() {
        MemberDto dto = new MemberDto("firstName", "lastName");
        Set<ConstraintViolation<MemberDto>> violations = validator.validate(dto);
        assertEquals(0, violations.size());
    }

    @Test
    @DisplayName("Should violate a constraint if first name is null")
    void ifFirstNameIsNull_thenViolateOneConstraint() {
        MemberDto dto = new MemberDto(null, "lastName");
        Set<ConstraintViolation<MemberDto>> violations = validator.validate(dto);
        assertEquals(1, violations.size());
    }

    @Test
    @DisplayName("Should violate a constraint if last name is null")
    void ifLastNameIsNull_thenViolateOneConstraint() {
        MemberDto dto = new MemberDto("firstName", null);
        Set<ConstraintViolation<MemberDto>> violations = validator.validate(dto);
        assertEquals(1, violations.size());
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "  "})
    @DisplayName("Should violate a constraint if first name is blank or empty")
    void isFirstNameBlank_ShouldReturnConstraintForEmptyOrBlankStrings(String firstName) {
        MemberDto dto = new MemberDto(firstName, "lastName");
        Set<ConstraintViolation<MemberDto>> violations = validator.validate(dto);
        assertEquals(1, violations.size());
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "  "})
    @DisplayName("Should violate a constraint if last name is blank or empty")
    void isLastNameBlank_ShouldReturnConstraintForEmptyOrBlankStrings(String lastName) {
        MemberDto dto = new MemberDto("firstName", lastName);
        Set<ConstraintViolation<MemberDto>> violations = validator.validate(dto);
        assertEquals(1, violations.size());
    }

}
