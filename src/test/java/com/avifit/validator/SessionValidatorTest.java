package com.avifit.validator;

import com.avifit.dto.SessionDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.time.LocalDateTime;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class SessionValidatorTest {

    private static Validator validator;

    @BeforeAll
    public static void setup() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    @DisplayName("Should not violate any constraint when session is valid")
    void ifSessionIsValid_thenNoConstraintViolated() {
        SessionDto dto = new SessionDto("Session", 10, "Coach", LocalDateTime.now(), LocalDateTime.now().plusHours(1));
        Set<ConstraintViolation<SessionDto>> violations = validator.validate(dto);
        assertEquals(0, violations.size());
    }

    @Test
    @DisplayName("Should violate a constraint if session name is null")
    void ifSessionNameIsNull_thenViolateOneConstraint() {
        SessionDto dto = new SessionDto(null, 10, "Coach", LocalDateTime.now(), LocalDateTime.now().plusHours(1));
        Set<ConstraintViolation<SessionDto>> violations = validator.validate(dto);
        assertEquals(1, violations.size());
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "  "})
    @DisplayName("Should violate a constraint if session name is blank or empty")
    void isSessionNameBlank_ShouldReturnTrueForEmptyOrBlankStrings(String sessionName) {
        SessionDto dto = new SessionDto(sessionName, 10, "Coach", LocalDateTime.now(), LocalDateTime.now().plusHours(1));
        Set<ConstraintViolation<SessionDto>> violations = validator.validate(dto);
        assertEquals(1, violations.size());
    }

    @Test
    @DisplayName("Should violate a constraint if number of places is null")
    void ifNumberOfPlacesIsNull_thenViolateOneConstraint() {
        SessionDto dto = new SessionDto("Session name", null, "Coach", LocalDateTime.now(), LocalDateTime.now().plusHours(1));
        Set<ConstraintViolation<SessionDto>> violations = validator.validate(dto);
        assertEquals(1, violations.size());
    }

    @Test
    @DisplayName("Should violate a constraint if number of places is too low")
    void ifNumberOfPlacesIsUnderTwo_thenViolateOneConstraint() {
        SessionDto dto = new SessionDto("Session name", 1, "Coach", LocalDateTime.now(), LocalDateTime.now().plusHours(1));
        Set<ConstraintViolation<SessionDto>> violations = validator.validate(dto);
        assertEquals(1, violations.size());
    }

    @Test
    @DisplayName("Should violate a constraint if coach is null")
    void ifCoachIsNull_thenViolateOneConstraint() {
        SessionDto dto = new SessionDto("Session name", 10, null, LocalDateTime.now(), LocalDateTime.now().plusHours(1));
        Set<ConstraintViolation<SessionDto>> violations = validator.validate(dto);
        assertEquals(1, violations.size());
    }

    @Test
    @DisplayName("Should violate a constraint if start date is null")
    void ifStartDateIsNull_thenViolateOneConstraint() {
        SessionDto dto = new SessionDto("Session name", 10, "Coach", null, LocalDateTime.now().plusHours(1));
        Set<ConstraintViolation<SessionDto>> violations = validator.validate(dto);
        assertEquals(1, violations.size());
    }

    @Test
    @DisplayName("Should violate a constraint if end date is null")
    void ifEndDateIsNull_thenViolateOneConstraint() {
        SessionDto dto = new SessionDto("Session name", 10, "Coach", LocalDateTime.now(), null);
        Set<ConstraintViolation<SessionDto>> violations = validator.validate(dto);
        assertEquals(1, violations.size());
    }


}
