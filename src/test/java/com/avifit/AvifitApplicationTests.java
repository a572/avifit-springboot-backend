package com.avifit;

import com.avifit.controller.MemberController;
import com.avifit.controller.SessionController;
import com.avifit.service.MemberService;
import com.avifit.service.SessionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class AvifitApplicationTests {

    @Autowired
    private SessionController sessionController;

    @Autowired
    private MemberController memberController;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private MemberService memberService;

    @Test
    void contextLoads() {
        assertThat(sessionController).isNotNull();
        assertThat(memberController).isNotNull();
        assertThat(sessionService).isNotNull();
        assertThat(memberService).isNotNull();
    }

}
