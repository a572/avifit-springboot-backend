package com.avifit.entity;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest
class SessionTest {

    @Test
    void shouldReturnFalseIfNotFull() {
        Session session = new Session();
        session.setNumberOfPlaces(10);
        assertFalse(session.isFull());
    }

    @Test
    void shouldReturnTrueIfFull() {
        Session session = new Session();
        session.setNumberOfPlaces(1);
        session.getRegisteredMembers().add(new Member());
        assertTrue(session.isFull());
    }

    @Test
    void shouldReturnTrueIfMoreThanFull() {
        Session session = new Session();
        session.setNumberOfPlaces(1);
        session.getRegisteredMembers().add(new Member());
        session.getRegisteredMembers().add(new Member());
        assertTrue(session.isFull());
    }

}
