package com.avifit.service;

import com.avifit.exception.SessionNotFoundException;
import com.avifit.repository.SessionRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@SpringBootTest
class SessionServiceTest {

    @Mock
    private SessionRepository sessionRepository;

    @InjectMocks
    private SessionService sessionService = new SessionService();

    @Test
    void should_throw_exception_when_get_by_id_not_found() {
        when(sessionRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(SessionNotFoundException.class, () -> sessionService.findById(1L));
    }
}
