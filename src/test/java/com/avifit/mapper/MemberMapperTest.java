package com.avifit.mapper;

import com.avifit.dto.MemberDto;
import com.avifit.entity.Member;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class MemberMapperTest {

    private final MemberMapper mapper = Mappers.getMapper(MemberMapper.class);

    @Test
    void whenConvertMemberEntityToMemberDto_thenCorrect() {
        Member entity = new Member();
        entity.setFirstName("First name");
        entity.setLastName("Last name");

        MemberDto dto = mapper.memberToMemberDto(entity);
        assertEquals(entity.getFirstName(), dto.firstName());
        assertEquals(entity.getLastName(), dto.lastName());
    }

    @Test
    void whenConvertMemberDtoToMemberEntity_thenCorrect() {
        MemberDto dto = new MemberDto("toto", "tata");

        Member entity = mapper.memberDtoToMember(dto);
        assertEquals(entity.getFirstName(), dto.firstName());
        assertEquals(entity.getLastName(), dto.lastName());
    }

}
