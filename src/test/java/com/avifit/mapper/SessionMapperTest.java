package com.avifit.mapper;

import com.avifit.dto.SessionDto;
import com.avifit.entity.Session;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class SessionMapperTest {

    private final SessionMapper mapper = Mappers.getMapper(SessionMapper.class);

    @Test
    void whenConvertSessionEntityToSessionDto_thenCorrect() {
        Session entity = new Session();
        entity.setName("Session");
        entity.setNumberOfPlaces(10);
        entity.setCoach("Coach");
        entity.setBeginning(LocalDateTime.now());
        entity.setEnd(LocalDateTime.now().plusHours(1));

        SessionDto dto = mapper.sessionToSessionDto(entity);
        assertEquals(entity.getName(), dto.name());
        assertEquals(entity.getNumberOfPlaces(), dto.numberOfPlaces());
        assertEquals(entity.getCoach(), dto.coach());
        assertEquals(entity.getBeginning(), dto.beginning());
        assertEquals(entity.getEnd(), dto.end());
    }

    @Test
    void whenConvertSessionDtoToSessionEntity_thenCorrect() {
        SessionDto dto = new SessionDto("Session", 10, "Coach", LocalDateTime.now(), LocalDateTime.now().plusHours(1));

        Session entity = mapper.sessionDtoToSession(dto);
        assertEquals(entity.getName(), dto.name());
        assertEquals(entity.getNumberOfPlaces(), dto.numberOfPlaces());
        assertEquals(entity.getCoach(), dto.coach());
        assertEquals(entity.getBeginning(), dto.beginning());
        assertEquals(entity.getEnd(), dto.end());
    }
}
