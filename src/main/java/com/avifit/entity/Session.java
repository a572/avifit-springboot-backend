package com.avifit.entity;

import com.avifit.shared.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "sessions")
public class Session {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private int numberOfPlaces;

    private String coach;

    @JsonFormat(pattern = Constants.FRENCH_LOCAL_DATE_TIME_FORMAT)
    private LocalDateTime beginning;

    @JsonFormat(pattern = Constants.FRENCH_LOCAL_DATE_TIME_FORMAT)
    private LocalDateTime end;

    @ManyToMany(mappedBy = "sessions", fetch = FetchType.LAZY)
    @JsonIgnoreProperties("sessions")
    private List<Member> registeredMembers = new ArrayList<>();

    public boolean isFull() {
        return this.registeredMembers.size() >= this.numberOfPlaces;
    }
}
