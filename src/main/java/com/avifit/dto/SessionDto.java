package com.avifit.dto;

import com.avifit.shared.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public record SessionDto(

        @NotBlank(message = "Session name is mandatory.")
        String name,

        @NotNull
        @Min(value = 2, message = "Number of places must be greater or equal to 2.")
        Integer numberOfPlaces,

        @NotNull(message = "Coach is mandatory.")
        String coach,

        @NotNull(message = "Beginning date is mandatory.")
        @JsonFormat(pattern = Constants.FRENCH_LOCAL_DATE_TIME_FORMAT)
        LocalDateTime beginning,

        @NotNull(message = "End date is mandatory.")
        @JsonFormat(pattern = Constants.FRENCH_LOCAL_DATE_TIME_FORMAT)
        LocalDateTime end
) {
}
