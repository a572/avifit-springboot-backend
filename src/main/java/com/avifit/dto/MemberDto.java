package com.avifit.dto;

import javax.validation.constraints.NotBlank;

public record MemberDto(

        @NotBlank(message = "First name cannot be null.")
        String firstName,

        @NotBlank(message = "Last name cannot be null.")
        String lastName
) {
}
