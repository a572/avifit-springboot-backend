package com.avifit.controller;

import com.avifit.dto.MemberDto;
import com.avifit.entity.Member;
import com.avifit.mapper.MemberMapper;
import com.avifit.service.MemberService;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/member")
public class MemberController {

    @Autowired
    private MemberService service;

    private MemberMapper mapper = Mappers.getMapper(MemberMapper.class);

    @GetMapping
    public ResponseEntity<Iterable<Member>> findAll() {
        Iterable<Member> members = service.findAll();
        return new ResponseEntity<>(members, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<Member> findById(@PathVariable Long id) {
        var member = service.findById(id);
        return new ResponseEntity<>(member, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Member> create(@Valid @RequestBody MemberDto memberDto) {
        var newMember = service.save(mapper.memberDtoToMember(memberDto));
        return new ResponseEntity<>(newMember, HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<Member> update(@PathVariable("id") Long id, @Valid @RequestBody MemberDto memberDto) {
        var oldMember = service.findById(id);
        var newMember = mapper.memberDtoToMember(memberDto);
        newMember.setId(oldMember.getId());
        service.save(newMember);
        return new ResponseEntity<>(newMember, HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<HttpStatus> deleteById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(service.deleteById(id));
    }

    @DeleteMapping
    public ResponseEntity<HttpStatus> deleteAll() {
        return new ResponseEntity<>(service.deleteAll());
    }

    @PutMapping("{memberid}/sessions/{sessionid}/register")
    public ResponseEntity<Member> registerToSession(@PathVariable("memberid") Long memberId, @PathVariable("sessionid") Long sessionId) {
        var updatedMember = service.registerToSession(memberId, sessionId);
        return new ResponseEntity<>(updatedMember, HttpStatus.OK);
    }

    @PutMapping("{memberid}/sessions/{sessionid}/unregister")
    public ResponseEntity<Member> unregisterToSession(@PathVariable("memberid") Long memberId, @PathVariable("sessionid") Long sessionId) {
        var updatedMember = service.unregisterToSession(memberId, sessionId);
        return new ResponseEntity<>(updatedMember, HttpStatus.OK);
    }

}
