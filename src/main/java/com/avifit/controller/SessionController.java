package com.avifit.controller;

import com.avifit.dto.SessionDto;
import com.avifit.entity.Session;
import com.avifit.mapper.SessionMapper;
import com.avifit.service.SessionService;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/session")
public class SessionController {

    @Autowired
    private SessionService service;

    private SessionMapper mapper = Mappers.getMapper(SessionMapper.class);

    @GetMapping
    public ResponseEntity<Iterable<Session>> findAll() {
        Iterable<Session> sessions = service.findAll();
        return new ResponseEntity<>(sessions, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<Session> findById(@PathVariable Long id) {
        var session = service.findById(id);
        return new ResponseEntity<>(session, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Session> create(@Valid @RequestBody SessionDto sessionDto) {
        var newSession = service.save(mapper.sessionDtoToSession(sessionDto));
        return new ResponseEntity<>(newSession, HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<Session> update(@PathVariable("id") Long id, @Valid @RequestBody SessionDto sessionDto) {
        var oldSession = service.findById(id);
        var newSession = mapper.sessionDtoToSession(sessionDto);
        newSession.setId(oldSession.getId());
        service.save(newSession);
        return new ResponseEntity<>(newSession, HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<HttpStatus> deleteById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(service.deleteById(id));
    }

    @DeleteMapping
    public ResponseEntity<HttpStatus> deleteAll() {
        return new ResponseEntity<>(service.deleteAll());
    }

}
