package com.avifit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvifitApplication {

    public static void main(String[] args) {
        SpringApplication.run(AvifitApplication.class, args);
    }
    
}
