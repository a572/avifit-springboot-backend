package com.avifit.mapper;

import com.avifit.dto.MemberDto;
import com.avifit.entity.Member;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface MemberMapper {

    @Mapping(source = "firstName", target = "firstName")
    @Mapping(source = "lastName", target = "lastName")
    MemberDto memberToMemberDto(Member member);

    @Mapping(source = "firstName", target = "firstName")
    @Mapping(source = "lastName", target = "lastName")
    Member memberDtoToMember(MemberDto memberDto);

}
