package com.avifit.mapper;

import com.avifit.dto.SessionDto;
import com.avifit.entity.Session;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SessionMapper {

    SessionDto sessionToSessionDto(Session session);

    Session sessionDtoToSession(SessionDto sessionDto);

}
