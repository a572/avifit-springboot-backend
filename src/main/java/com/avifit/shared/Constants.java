package com.avifit.shared;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Constants {

    public static final String FRENCH_LOCAL_DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm";

}
