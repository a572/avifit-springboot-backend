package com.avifit.service;

import com.avifit.entity.Session;
import com.avifit.exception.SessionNotFoundException;
import com.avifit.repository.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class SessionService {

    @Autowired
    private SessionRepository repository;

    public Iterable<Session> findAll() {
        return repository.findAll();
    }

    public Session findById(Long id) {
        return repository.findById(id).orElseThrow(SessionNotFoundException::new);
    }

    public Session save(Session session) {
        return repository.save(session);
    }

    public HttpStatus deleteById(Long id) {
        var session = repository.findById(id).orElseThrow(SessionNotFoundException::new);
        repository.delete(session);
        return HttpStatus.NO_CONTENT;
    }

    public HttpStatus deleteAll() {
        repository.deleteAll();
        return HttpStatus.NO_CONTENT;
    }

}
