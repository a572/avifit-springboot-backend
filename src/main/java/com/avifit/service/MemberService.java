package com.avifit.service;

import com.avifit.entity.Member;
import com.avifit.exception.*;
import com.avifit.repository.MemberRepository;
import com.avifit.repository.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class MemberService {

    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private SessionRepository sessionRepository;

    public Iterable<Member> findAll() {
        return memberRepository.findAll();
    }

    public Member findById(Long id) {
        return memberRepository.findById(id).orElseThrow(MemberNotFoundException::new);
    }

    public Member save(Member member) {
        return memberRepository.save(member);
    }

    public HttpStatus deleteById(Long id) {
        var member = memberRepository.findById(id).orElseThrow(MemberNotFoundException::new);
        memberRepository.delete(member);
        return HttpStatus.NO_CONTENT;
    }

    public HttpStatus deleteAll() {
        memberRepository.deleteAll();
        return HttpStatus.NO_CONTENT;
    }

    public Member registerToSession(Long memberId, Long sessionId) {
        var member = memberRepository.findById(memberId).orElseThrow(MemberNotFoundException::new);
        var session = sessionRepository.findById(sessionId).orElseThrow(SessionNotFoundException::new);

        if (member.getSessions().contains(session)) {
            throw new MemberAlreadyRegisteredToSessionException();
        } else if (session.isFull()) {
            throw new SessionAlreadyFullException();
        }

        member.getSessions().add(session);
        return memberRepository.save(member);
    }

    public Member unregisterToSession(Long memberId, Long sessionId) {
        var member = memberRepository.findById(memberId).orElseThrow(MemberNotFoundException::new);
        var session = sessionRepository.findById(sessionId).orElseThrow(SessionNotFoundException::new);

        if (!member.getSessions().contains(session)) {
            throw new MemberNotRegisteredToSessionException();
        }

        member.getSessions().remove(session);
        return memberRepository.save(member);
    }

}
